console.log("readingListAct2");

let tasks = [
	"shower",
	"eat breakfast",
	"got to work",
	"go home",
	"go to sleep"
]
// Create an array of students with the following value.
// 			let students = ["John", "Jane", "Joe"];
// 			Find the following information using array methods: mutators and accessors: 

// let students = ["John", "Jane", "Joe"]
// // 			1.) Total size of an array.
// console.log("the total size the array is " + students.length);

// // 			2.) Adding a new element at the beginning of an array. (Add Jack)
// // mutator
// students.unshift("Jack");
// console.log("Jack is added in the beginning of the array");
// console.log(students);

// // 			3.) Adding a new element at the end of an array. (Add Jill)
// // push method
// students.push("Jill");
// console.log("Jill is added in the end of array");
// console.log(students);

// // 			4.) Removing an existing element at the beginning of an array.
// // shift method - removed 0 or first index
// students.shift();
// console.log("Jack was removed in the beginning of array");
// console.log(students);

// // 			5.) Removing an existing element at the end of an array.
// students.pop();
// console.log("Jill was removed in then end of an array");
// console.log(students);

// // 			6.) Find the index number of the last element in an array
// console.log("The last index of the array is " +(students.length-1));

// 			7.) Print each element of the array in the browser console.
// for loop method
// array starts at 0 (i++ because starts from 0)
// length of students is 3
// will only stop if 3 < 3 false
// this is first method
// for(i = 0; i < students.length; i++){
// 	console.log(students[i]);
// 	// i to get current index

// }

// iteration array method
// students.forEach(function(student){
// 	console.log(student);
// })

// Iteration methods are actually loops designed to perform repetitive tasks on array.
// useful for manipulating array data resulting in complex task.

// forEach() 
// similar to a for loop that iterates on each array element 

/*
		Syntax:
			arrayName.forEach(function(indivElement){
			statment / code block 
			})
*/
// it's up to you how you will rename task or x variable naming as long inside the function.

// Using for each with conditional statements 
// let filteredTasks = [];



// tasks.forEach(function(task){
// 	// "task" parameter each element of the "tasks" array
// 	// console.log(x)
// does not work on Return statements

// 	// if the elements length is greater than 10 characters it wil be stored inside the filteredTasks variable
// 		if(task.length > 10){
// 			// add element to the filteredTasks array 
// 			filteredTasks.push(task);
// 		}
// })
// console.log("Result of filteredTasks");
// console.log(filteredTasks);



// MAP method map()
// iterates on each element  and returns new array with different values
// depending on the result of the function's operation

/*
		let / const resultArray = arrayName.map(function (indivElement){
				statement / code block 
				return statement
		})
*/
let numbers = [1, 2, 3, 4, 5];

// let numberMap = numbers.map(function(number){
// 		return number * number;

// })
// console.log(numbers);
// console.log("Result of map method");
// console.log(numberMap);

// let taskMap = tasks.map(function(task){
// 		if(task.length > 10){
// 			return task;
// 		}
// })

// console.log(taskMap);
// undefined are the > 10 less than 10


// EVERY method  every()
// check if all elements in an array meet the given condition 
// return a true value if all elements meet the condition

// let allValid = numbers.every(function(number){
// 		return (number < 3);

// })
// console.log("Reuslt of every method");
// console.log(allValid);


// // SOME method some() 
// // check if atleast 1 element in the array meets the given condition
// // return a "true" value if one element meets the condition
// let someValid = numbers.some(function(number){
// 		return (number < 3);
// })
// console.log("return of some method");
// console.log(someValid);

// // combining the returned result from every / some method may be used in other staetments
// // to perform consecutive result

// if(someValid){
// 	// will display less than 3
// 	// if changed to allValid console will not show will go to else
// 	console.log("Some numbers in the array are greater than 3");

// }

// FILTER method filter()
// returns a new array 
// filtering using forEach method
// returns an empty array if no elements were found 
/*
			let/const resultArray = arrayName.filter(function(indivElement){
				return expression / condition 
			})
*/

// let filteredNumbers = []
// numbers.forEach(function(number){
// 	if (number < 3){
// 		filteredNumbers.push(number);
// 	}
// })
// console.log("Result of filterNumbers using forEach:")
// console.log(filteredNumbers);

let filterValid = numbers.filter(function(number){
	return (number > 1);
});
console.log("Result of the filter method:")
console.log(filterValid);

// =============================
// let taskFilter = tasks.filter(function(task){
// 	return (task > 10);
// })
// console.log(taskFilter);
// =============================


let products = ['Mouse', 'keyboard', 'laptap', 'monitor'];
	// includes ()
	// determines whether an array includes a certain value among array elements 
	// return boolean and case sensitive
	// includes checks if the character or series characters exist in the string
// 	console.log(products);
// 	console.log(products.includes('mouse'));
// // declare another string
// let word = 'Hello'

// console.log(word.toLowerCase().includes('hlo'));

// METHODS can be "chained"
// it can be used one after another.
// inside method before outside method

// if we have mouse inside the product of array 
let filteredProducts = products.filter(function(product){
	// return (product.toLowerCase() == 'mouse');
/*
			using includes checks if an array elements contains letter 'a'
			mouse = false
			keyboard = true
			laptap = true
			monitor = false
*/
	console.log(product.toLowerCase().includes('a'));
	return product.toLowerCase().includes('a');
	// all products with letter o will all show
})

console.log(filteredProducts);

// REDUCE reduce()
// evaluates elements from left to right and returns / reduces the array into a single value

/*
			let/const = resultArray = arrayName.reduce(function(accumulatorl,
			currentValue){
					
					return expression/operation
			})

			accumulator parameter stores the result for every iteration of the loop
			-current value /next element in the array that is evaluated 
			in each iteration of the loop.
*/
let iteration = 0;
let reduceArray = numbers.reduce(function(acc, cur){
	console.warn('current iteration' + ++iteration);
	console.log('accumulator' +acc);
	console.log('currentVaue' +cur);
	return acc + cur;
	// used to track the current iteration count 
})
//                                      1+2+3+4+5 = 15
console.log("result of reduce method " + reduceArray);


// REDUCING STRING ARRAY
let list = ['hello', 'again', 'world'];

let reduceJoin = list.reduce(function(x, y){
	console.warn('current iteration: ' + iteration++);
	console.log('accumulator: ' +x);
	console.log('currentVaue: ' +y);
		return x + " " + y;
})
console.log("result of reduce method in string " +reduceJoin);

// ============================
function printHello(){
	console.log("Hello world");
}
// ==============================

// MULTI-DIMENTIONAL  A R R A Y ===========================
// it isk useful for storing complex data structures
// Array within an Array 
/*
			Syntax: 

			let / const arrayName = [[] []];

*/
//  2 DIMENSIONAL A R R A Y==============================
//  2 x 3 (2 dimensional array)

let twoDim = [[2,4,6], [8, 10, 12], [23, 34, 50]];

//                0 is outer | 1 is inner array

console.table(twoDim);
console.log(twoDim[0][1]);












